# rb
![Magical](logo/rb.svg)

# Dependencies
- [just](https://github.com/casey/just)
- [Postgres](https://www.postgresql.org/)
- [Rust nightly and cargo](https://www.rust-lang.org/)
- [sqlx-cli](https://github.com/launchbadge/sqlx/tree/HEAD/sqlx-cli)
- [Python 3](https://www.python.org/downloads/)
- [schedule](https://pypi.org/project/schedule/)
- [proot](https://proot-me.github.io/)
- OpenSSL
- Optional (for command `just db diagram`):
    - fakeroot
    - GCC
    - Graphviz
    - eralchemy
    - psycopg2
    - SQLAlchemy

## Flavor: Arch Linux
```sh
# From Arch Linux repos.
sudo pacman -S fakeroot python rustup just postgresql postgresql-libs sqlx-cli openssl

# From AUR.
trizen -S proot-bin

# From suspicious servers.
rustup default nightly
pip install schedule

# Optional.
sudo pacman -S graphivz gcc fakeroot
pip install "sqlalchemy<1.4" eralchemy psycopg2
```

# Usage

## First Start
```sh
# Load environmental variables
cp example.env .env
source .env

# Create $DATA_VOLUME/ and target/
just init

# During development better store data on RAM for frequent uses of `just db reset`.
# Don't ever do this in production.
sudo mount -t tmpfs -o size=500m  swap $DATA_VOLUME
# You can mount Rust builds in RAM as well if you feel comfortable.
# This can speed up compilation time.
# Don't ever do this in production.
sudo mount -t tmpfs -o size=6000m swap target/

# Build media maintainer extension for the database.
just media_maintainer build

# Reset and start the database.
just db reset
just db start &

# Start scheduler
just scheduler start &

# Generate self-signed pair of cert.pem and key.pem in tls/
just gentls

# Build and start frontend
just frontend syncdb
just frontend build
just frontend start &
```

## Cleanup
```sh
# Stop services.
just stop

# Clean $DATA_VOLUME/ and target/.
just clean

# Free RAM.
sudo umount $DATA_VOLUME
sudo umount target/
```