//! Common [Error] type for the whole application

/// Errors sorted by time of possible occurence after starting the application.
#[derive(Debug, thiserror::Error)]
#[non_exhaustive]
pub enum Error {
    #[error(transparent)]
    EnvVar(#[from] std::env::VarError),
    #[error(transparent)]
    ParseNetAddr(#[from] std::net::AddrParseError),
    #[error(transparent)]
    ParseInt(#[from] std::num::ParseIntError),
    #[error(transparent)]
    ParseLogLevel(#[from] log::ParseLevelError),
    #[error(transparent)]
    ParseUri(#[from] poem::http::uri::InvalidUri),
    #[error("could not parse \"{0}\" as a byte size, e.g. \"419.0 MB\"")]
    ParseByteSize(String),
    #[error(transparent)]
    ParseLangId(#[from] unic_langid::LanguageIdentifierError),
    #[error(transparent)]
    Sqlx(#[from] sqlx::Error),
    #[error(transparent)]
    Fluent(#[from] fluent::FluentError),
    #[error(transparent)]
    Tera(#[from] tera::Error),
    #[error(transparent)]
    Io(#[from] std::io::Error),
    #[error(transparent)]
    Image(#[from] image::error::ImageError),
}
