//! Frontend for rb separated as endpoints in [shop] and [editor].

#![allow(unused_variables, unused_imports, unused_mut, dead_code)]
#![feature(default_free_fn, associated_type_defaults, arc_unwrap_or_clone)]
#![feature(duration_constants, panic_info_message, async_closure, io_error_other)]

pub mod config;
pub mod constituent;
pub mod editor;
pub mod error;
pub mod media;
pub mod product;
pub mod repo;
pub mod server;
pub mod shop;
pub mod slug;
pub mod text;
pub mod time;
pub mod translation;
pub mod voc;

use crate::config::Config;
/// Common [Error] type.
pub use crate::error::Error;
use std::env;

/// Common code type for content types.
pub type Code = String;
/// Common ID type for content types.
pub type Id = i64;
/// Common type for binary data.
pub type BinaryData = Vec<u8>;

/// The famous main function.  
/// Loads config, starts logging and runs the server.  
/// Hopefully exits gracefully finally.
#[tokio::main]
async fn main() -> Result<(), Error> {
    // Load config.
    let config = Config::try_from_env()?;

    // Set logging.
    env::set_var("RUST_LOG", config.log_level.as_str());
    tracing_subscriber::fmt::init();

    // Run server.
    server::run(config).await
}
