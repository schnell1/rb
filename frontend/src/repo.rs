//! [Repo] contains all content and manages internal caches.

use crate::{
    media::{try_get_media, Medium, MediumGet, MediumId},
    slug::{try_get_all_slugs, try_get_slug, Slug, SlugGetOne, SlugItem},
    translation::TranslationCode,
    voc::VocByTranslation,
    Error, Id,
};
use moka::future::Cache as FutureCache;
use sqlx::PgPool;
use std::{
    convert::Infallible,
    path::{Path, PathBuf},
    sync::Arc,
    time::Duration,
};

/// Cache which can store [None] for visited keys without value.
type Cache<K, V> = FutureCache<K, Option<Arc<V>>>;

/// Repository for all content.
///
/// Maintains data by querying database, reading files and caching the results and requests.
///
/// # Caching behaviour
/// Tries to find items in cache. If not found in cache, search in database.
/// If database doesn't find a value, [None] value is stored in cache for the given item key.  
/// [None] is stored to prevent further requests to the database.  
///   
/// If the key isn't asked for in a long time or cache capacity is reached,
/// unpopular key-value pairs will get dropped automatically from cache.
#[allow(clippy::type_complexity)]
pub struct Repo {
    /// This pooled database connection is public to be conveniently [clone][std::clone::Clone::clone]d from outside.
    pub pg_pool: Arc<PgPool>,
    media_volume: Arc<PathBuf>,
    media: Arc<Cache<MediumId, Medium>>,
    voc: Arc<VocByTranslation>,
    slugs: Arc<Cache<Slug, (Option<TranslationCode>, SlugItem)>>,
}

impl Repo {
    /// Creates new repo from pooled database connection.
    pub fn new<P: AsRef<Path>>(pg_pool: PgPool, media_volume: P) -> Self {
        Self {
            pg_pool: Arc::new(pg_pool),
            media_volume: Arc::new(media_volume.as_ref().to_path_buf()),
            media: Arc::new(
                FutureCache::builder()
                    .max_capacity(10_000)
                    .time_to_live(Duration::from_secs(60 * 60 * 10))
                    .time_to_idle(Duration::from_secs(60 * 60))
                    .build(),
            ),
            voc: Arc::new(VocByTranslation::default()),
            slugs: Arc::new(
                FutureCache::builder()
                    .max_capacity(40_000)
                    .time_to_live(Duration::from_secs(60 * 2))
                    .time_to_idle(Duration::from_secs(60 * 60 * 10))
                    .build(),
            ),
        }
    }

    /// Reinitializes items.
    pub async fn reinit(&self) -> Vec<Error> {
        let mut errors = Vec::new();

        log::debug!("Reinitializing cache");

        self.slugs.invalidate_all();
        let new_slugs = try_get_all_slugs(&*self.pg_pool).await;

        match new_slugs {
            Ok(new_slugs) => {
                for slug in new_slugs {
                    self.slugs
                        .insert(
                            slug.slug,
                            Some(Arc::new((slug.translation_code, slug.item))),
                        )
                        .await;
                }
            }
            Err(err) => errors.push(err),
        }

        self.voc.clear();
        let voc_init_errs = self.voc.init(&*self.pg_pool).await;

        for err in voc_init_errs {
            errors.push(err);
        }

        errors
    }

    /// Shrink itself to the given size in bytes.
    pub async fn shrink_to(&self, size: u64) {
        // config.cache_size_max
        todo!()
    }

    /// Gets the item and translation code for a given slug.
    pub async fn get_slug<S: AsRef<str>>(
        &self,
        slug: S,
    ) -> Option<Arc<(Option<TranslationCode>, SlugItem)>> {
        let _pg_pool = self.pg_pool.clone();
        let slug = slug.as_ref();

        let res = self
            .slugs
            .try_get_with(slug.to_string(), async move {
                let res = try_get_slug(&*self.pg_pool, slug).await;

                match res {
                    Ok(Some(SlugGetOne {
                        item,
                        translation_code,
                    })) => Ok(Some(Arc::new((translation_code, item)))),
                    _ => Result::<_, Infallible>::Ok(None),
                }
            })
            .await;

        res.ok()?
    }

    /// Gets a [Medium] for a given ID.
    pub async fn get_medium(&self, id: Id) -> Option<Arc<Medium>> {
        let _pg_pool = self.pg_pool.clone();

        let res = self
            .media
            .try_get_with(id, async move {
                let res = try_get_media(
                    &*self.pg_pool,
                    &*self.media_volume,
                    vec![id],
                    None,
                    None,
                    false,
                )
                .await;

                match res {
                    Ok(mut media) => {
                        if media.len() == 1 {
                            let (MediumGet { medium, .. }, _errors) = media.pop().unwrap();
                            Ok(Some(Arc::new(medium)))
                        } else {
                            Result::<_, Infallible>::Ok(None)
                        }
                    }
                    _ => Result::<_, Infallible>::Ok(None),
                }
            })
            .await;

        res.ok()?
    }
}
