//! Server function [run].

use crate::{
    config::{Config, HttpsConfig},
    editor,
    repo::Repo,
    shop, Error,
};
use async_stream::stream;
use poem::{
    self,
    endpoint::{self},
    get, handler,
    listener::{Listener, TcpListener},
    Endpoint, Route, Server,
};
use sqlx::PgPool;
use std::{net::SocketAddr, sync::Arc, time::Duration};
use tokio::{join, spawn, time::sleep};

/// Runs the HTTPS endpoints for shop and editor.
pub async fn run(config: Config) -> Result<(), Error> {
    let pg_pool = PgPool::connect(&config.database_url.to_string()).await?;

    let repo = Arc::new(Repo::new(pg_pool.clone(), config.media_volume));
    let _ = repo.reinit().await;

    let shop_app = shop::app("templates/shop/**/*", config.shop.clone(), repo.clone(), config.media_resize_steps.clone()).await?;

    let editor_app =
        editor::app("templates/editor/**/*", config.editor.clone(), repo.clone()).await?;

    let same_socket = HttpsConfig::same_socket(&config.editor, &config.shop);
    let same_host = HttpsConfig::same_host(&config.editor, &config.shop);
    let same_socket_different_routes = same_socket && same_host;
    let same_socket_different_hosts = same_socket && !same_host;

    let listener = |config: HttpsConfig| {
        TcpListener::bind(SocketAddr::from((config.ip, config.port))).rustls(stream! {
            loop {
                if let Ok(tls_config) = config.load_tls() {
                    yield tls_config;
                }
                sleep(Duration::from_secs(60 * 30)).await;
            }
        })
    };

    let common_config = config.shop.clone();

    if same_socket_different_routes {
        let common_app = Route::new()
            .nest("/shop", shop_app)
            .nest("/editor", editor_app);

        let common_server = Server::new(listener(common_config)).run(common_app);

        match common_server.await {
            Ok(()) => Ok(()),
            Err(err) => Err(err.into()),
        }
    } else if same_socket_different_hosts {
        #[handler]
        async fn redirect() {
            todo!()
        }

        let shop_app = Arc::new(shop_app);
        let editor_app = Arc::new(editor_app);
        let shop_config = Arc::new(config.shop);
        let editor_config = Arc::new(config.editor);

        let common_app = Box::new(endpoint::make(move |req| {
            let shop_app = shop_app.clone();
            let editor_app = editor_app.clone();
            let shop_config = shop_config.clone();
            let editor_config = editor_config.clone();

            async move {
                let shop_app = shop_app.clone();
                let editor_app = editor_app.clone();

                match req.uri().host() {
                    Some(hostname) => {
                        if hostname == shop_config.hostname {
                            shop_app.call(req).await
                        } else if hostname == editor_config.hostname {
                            editor_app.call(req).await
                        } else {
                            Route::new().at("/", get(redirect)).call(req).await
                        }
                    }
                    _ => Route::new().at("/", get(redirect)).call(req).await,
                }
            }
        }));

        let common_server = Server::new(listener(common_config)).run(common_app);

        match common_server.await {
            Ok(()) => Ok(()),
            Err(err) => Err(err.into()),
        }
    // serve with separate tasks
    } else {
        let shop_server =
            spawn(async move { Server::new(listener(config.shop)).run(shop_app).await });

        let editor_server =
            spawn(async move { Server::new(listener(config.editor)).run(editor_app).await });

        let (shop_err, editor_err) = join!(shop_server, editor_server);
        let (shop_err, editor_err) = (shop_err.unwrap(), editor_err.unwrap());

        // TODO: Better error handling. Currently this is a premature workaround.
        match (shop_err, editor_err) {
            (Err(shop_err), Err(editor_err)) => Err(Error::from(std::io::Error::other(format!(
                "{}, {}",
                shop_err, editor_err
            )))),
            (Err(shop_err), Ok(())) => Err(shop_err.into()),
            (Ok(()), Err(editor_err)) => Err(editor_err.into()),
            (Ok(()), Ok(())) => Ok(()),
        }
    }
}
