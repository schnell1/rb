//! Shop endpoint created by [app] and various handler functions.

use crate::{
    config::{BaseUrl, HttpsConfig, MediaResizeSteps},
    media::MediumItem,
    repo::Repo,
    slug::SlugItem,
    Error,
};

use poem::{
    get, handler,
    http::{
        header::{CACHE_CONTROL, CONTENT_TYPE, LAST_MODIFIED},
        HeaderMap, HeaderValue, StatusCode,
    },
    middleware::AddData,
    middleware::{AddDataEndpoint, CookieJarManagerEndpoint},
    session::{CookieConfig, ServerSession, ServerSessionEndpoint, Session},
    web::{cookie::SameSite, Data, IntoResponse, Path, Redirect},
    EndpointExt, Request, Response, Route,
};
use poem_dbsession::{sqlx::PgSessionStorage, DatabaseConfig};
use rand::random;
use serde::Deserialize;
use std::{sync::Arc, time::Duration};
use tera::Tera;

/// Endpoint type returned from [app].
pub type ShopEndpoint = AddDataEndpoint<AddDataEndpoint<CookieJarManagerEndpoint<ServerSessionEndpoint<PgSessionStorage, AddDataEndpoint<AddDataEndpoint<Route, Arc<Tera>>, Arc<Repo>>>>, BaseUrl>, Arc<MediaResizeSteps>>;

/// Creates an endpoint for the shop with given configuration and dependencies.
pub async fn app(
    templates_glob: impl Into<String>,
    https_config: HttpsConfig,
    repo: Arc<Repo>,
    media_resize_steps: MediaResizeSteps,
) -> Result<ShopEndpoint, Error> {
    let templates_glob = templates_glob.into();
    let templates = Arc::new(Tera::new(&templates_glob)?);
    let base_url = https_config.base_url();
    let media_resize_steps = Arc::new(media_resize_steps);

    let session_storage = PgSessionStorage::try_new(
        DatabaseConfig::new().table_name("shop_sessions"),
        (*repo.pg_pool).clone(),
    )
    .await
    .unwrap();

    let app = Route::new()
        .at("/", get(index).post(index))
        .at("/:slug", get(find).post(find))
        .with(AddData::new(templates.clone()))
        .with(AddData::new(repo.clone()))
        .with(ServerSession::new(
            CookieConfig::new()
            .name("session")
            .secure(true)
            .domain(https_config.host())
            .same_site(SameSite::Strict)
            .max_age(Duration::from_secs(2 * 60 * 60)),
            session_storage,
        ))
        .with(AddData::new(base_url))
        .with(AddData::new(media_resize_steps));

    Ok(app)
}

/// Handles request for `/`.
#[handler]
async fn index(
    _req: &Request,
    _session: &Session,
    templates: Data<&Arc<Tera>>,
    _repo: Data<&Arc<Repo>>,
) -> Response {
    let context = tera::Context::new();
    let body = templates.render("index.html", &context);
    let mut headers = HeaderMap::new();
    headers.insert(CONTENT_TYPE, HeaderValue::from_static("text/html"));

    match body {
        Ok(body) => (StatusCode::OK, headers, body).into_response(),
        Err(error) => {
            return (
                StatusCode::INTERNAL_SERVER_ERROR,
                headers,
                format!("<pre>{:#?}</pre>", error),
            )
                .into_response();
        }
    }
}

#[derive(Deserialize)]
struct ShopFindMedia {
    pub width: Option<u32>,
    pub height: Option<u32>,
}

/// Handles requests for `/:slug`.
#[handler]
async fn find(
    Path(slug): Path<String>,
    req: &Request,
    _templates: Data<&Arc<Tera>>,
    repo: Data<&Arc<Repo>>,
    _session: &Session,
    base_url: Data<&BaseUrl>,
    media_resize_steps: Data<&Arc<MediaResizeSteps>>,
) -> Response {
    let redirect_to_index = || {
        let base_url: String = base_url.0.clone().into();

        match (random::<u8>() % 4) + 1 {
            1 => Redirect::moved_permanent(base_url),
            2 => Redirect::permanent(base_url),
            3 => Redirect::see_other(base_url),
            _ => Redirect::temporary(base_url),
        }
        .into_response()
    };

    let maybe_slug = repo.get_slug(slug).await;

    if let Some(slug) = maybe_slug {
        let (_translation_code, slug_item) = &*slug;

        match slug_item {
            SlugItem::Page(ref code) => {
                let mut headers = HeaderMap::new();
                headers.insert(CONTENT_TYPE, HeaderValue::from_static("text/html"));
                let response =
                    (StatusCode::OK, headers, format!("<pre>{code}</pre>")).into_response();

                match code.as_str() {
                    "order" => response,
                    "legal" => response,
                    "login" => response,
                    _ => response,
                }
            }
            // TODO: check ?width=$width or else ?height=$height for resized versions
            SlugItem::Medium(id) => {
                let mut headers = HeaderMap::new();
                let medium = repo.get_medium(*id).await;

                if let Some(medium) = medium {
                    match &*medium.item {
                        MediumItem::Other => redirect_to_index(),
                        MediumItem::Image { image: _, .. } => {
                            let data = {
                                if let Ok(query) = req.params::<ShopFindMedia>() {
                                    let closest = |length| {
                                        match length {
                                            Some(length) => Some(media_resize_steps.closest(length)),
                                            None => None,
                                        }
                                    };

                                    if query.width.is_some() || query.height.is_some() {
                                        let width = closest(query.width);
                                        let height = closest(query.height);
                                        
                                        medium.get_resize(width, height, None).await
                                    } else {
                                        medium.data.clone()
                                    }
                                } else {
                                    None
                                }
                            };

                            if let Some(data) = data {
                                if let Some(mime_type) = &medium.mime_type {
                                    headers.insert(CONTENT_TYPE, mime_type.parse().unwrap());
                                    headers.insert(
                                        CACHE_CONTROL,
                                        "max-age: 31536000".try_into().unwrap(),
                                    );
                                    headers.insert(
                                        LAST_MODIFIED,
                                        medium.updated_at.header_value.clone(),
                                    );
                                    // TODO: check IfModifiedSince, if true, check updated_at, then send empty body or else data.
                                }
                                (StatusCode::OK, headers, (**data).to_vec()).into_response()
                            } else {
                                redirect_to_index()
                            }
                        }
                    }
                } else {
                    redirect_to_index()
                }
            }
            _item => redirect_to_index(),
        }
    } else {
        redirect_to_index()
    }
}
