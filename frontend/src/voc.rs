//! Vocabulary types.  
//!   
//! [VocByTranslation] is the main interface based on [FluentBundle] with messages stored in the database.

use crate::{
    translation::{TranslationCode, LANG_ID_UNDEFINED},
    Code, Error,
};
use dashmap::DashMap;
use fluent_bundle::{bundle::FluentBundle, FluentArgs, FluentResource};
use indoc::indoc;
use intl_memoizer::concurrent::IntlLangMemoizer;
use sqlx::{query_as, FromRow, PgPool};
use std::{
    collections::{HashMap, HashSet},
    hash::Hash,
};

/// Message code.
pub type MessageCode = Code;
/// Message.
pub type Message = String;
/// FTL messages.
pub type Ftl = String;
/// Concurrent [FluentBundle].
pub type Bundle = FluentBundle<FluentResource, IntlLangMemoizer>;

/// Statement for selecting the vocabulary sorted by translation codes to be parsed as FTL messages.
pub static VOC_BY_TRANSLATION_SELECT: &str = indoc! {"
    SELECT
        translation_code,
        array_agg(message_code || ' = ' || message) messages
    FROM
        voc
    GROUP BY
        translation_code
"};

/// Select record for [VocByTranslation].
#[derive(FromRow)]
pub struct VocByTranslationSelect {
    pub translation_code: TranslationCode,
    pub messages: Vec<Ftl>,
}

/// Stores all vocabulary.
#[derive(Default)]
pub struct VocByTranslation(DashMap<TranslationCode, Bundle>);

impl VocByTranslation {
    /// Clears all entries.
    pub fn clear(&self) {
        self.0.clear()
    }

    /// Fetches the whole vocabulary from the database.
    pub async fn init(&self, pg_pool: &PgPool) -> Vec<Error> {
        let rows = query_as::<_, VocByTranslationSelect>(VOC_BY_TRANSLATION_SELECT)
            .fetch_all(pg_pool)
            .await;

        let mut errors = Vec::new();

        if let Err(err) = rows {
            errors.push(Error::Sqlx(err));
        } else {
            let rows = rows.unwrap();

            for row in rows {
                let lang_id = row
                    .translation_code
                    .parse()
                    .unwrap_or_else(|_| LANG_ID_UNDEFINED.clone());
                let mut bundle = FluentBundle::new_concurrent(vec![lang_id]);

                for message in row.messages {
                    let resource = FluentResource::try_new(message);

                    match resource {
                        Err((_resource, errs)) => {
                            for err in errs {
                                errors.push(Error::Fluent(fluent::FluentError::ParserError(err)));
                            }
                        }
                        Ok(resource) => {
                            if let Err(errs) = bundle.add_resource(resource) {
                                for err in errs {
                                    errors.push(Error::Fluent(err));
                                }
                            }
                        }
                    }
                }

                let _ = self.0.insert(row.translation_code, bundle);
            }
        }

        errors
    }

    /// Tries to find messages for given message codes, arguments and translation codes.
    pub fn get_messages_for_translations<K, S>(
        &self,
        message_codes_with_args: HashMap<K, HashMap<K, S>>,
        translation_codes: HashSet<K>,
    ) -> HashMap<MessageCode, HashMap<TranslationCode, Message>>
    where
        S: AsRef<str>,
        K: Hash + Eq + AsRef<str>,
    {
        let mut translated_messages = HashMap::default();

        for translation_code in translation_codes {
            if let Some(bundle) = self.0.get(translation_code.as_ref()) {
                for (message_code, args) in &message_codes_with_args {
                    if let Some(message) = bundle.get_message(message_code.as_ref()) {
                        if let Some(pattern) = message.value() {
                            let mut errors = vec![];

                            let args = FluentArgs::from_iter(
                                args.iter().map(|(arg, val)| (arg.as_ref(), val.as_ref())),
                            );

                            let message = bundle.format_pattern(pattern, Some(&args), &mut errors);

                            let translations = translated_messages
                                .entry(message_code.as_ref().to_string())
                                .or_insert_with(HashMap::new);
                            let _translation = translations
                                .entry(translation_code.as_ref().to_string())
                                .or_insert_with(|| message.to_string());
                        }
                    }
                }
            }
        }

        translated_messages
    }

    /// Tries to find message for given message code and translation code.
    pub fn get_message<K, S>(
        &self,
        message_code: S,
        translation_code: K,
        args: HashMap<K, S>,
    ) -> Option<Message>
    where
        S: AsRef<str>,
        K: Hash + Eq + AsRef<str>,
    {
        let args = args
            .iter()
            .map(|(arg, val)| (arg.as_ref(), val.as_ref()))
            .collect();

        let message_codes_with_args = HashMap::from([(message_code.as_ref(), args)]);
        let translation_codes = HashSet::from([translation_code.as_ref()]);

        let messages_with_translations =
            self.get_messages_for_translations(message_codes_with_args, translation_codes);

        if let Some((_message_code, translated_messages)) = messages_with_translations.iter().next()
        {
            if let Some((_translation_code, message)) = translated_messages.iter().next() {
                return Some(message.to_string());
            }
        }

        None
    }
}
