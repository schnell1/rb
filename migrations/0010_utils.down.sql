DROP DOMAIN rb.postal_address;
DROP DOMAIN rb.email;

DROP FUNCTION rb.create_texts_default;
DROP FUNCTION rb.create_texts;
DROP FUNCTION rb.create_media;

DROP FUNCTION set_updated_at;
DROP FUNCTION set_published_at;
DROP FUNCTION add_created;
DROP FUNCTION add_updated;
DROP FUNCTION add_published;

DROP FUNCTION rb.register_slugs;
DROP TYPE rb.fkey_slug;

DROP FUNCTION rb.create_check_exactly_one_dst;

DROP VIEW rb.pkeys;
DROP TYPE rb.pkey;

DROP VIEW rb.fkeys_grouped_by_src;
DROP TYPE rb.fkey_grouped_by_src;

DROP VIEW fkeys_grouped_by_dst;
DROP TYPE fkey_grouped_by_dst;

DROP VIEW rb.fkeys;
DROP TYPE rb.fkey;

DROP TYPE rb.col;

--DROP EXTENSION multicorn;