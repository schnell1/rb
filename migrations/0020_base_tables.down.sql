-- `slugs` is actually dropped in 999_finish.down.sql as first revert step
-- because custom tables may use it.

DROP TABLE rb.aspects_texts;
DROP TABLE rb.aspects;

DROP TABLE rb.media_texts;
DROP TABLE rb.media;
DROP TABLE rb.media_mime_types;

DROP TABLE rb.locales_texts;
DROP TABLE rb.locales_fallbacks;
DROP TABLE rb.locales;
