#!/usr/bin/python
import jinja2
from demo.data import data
from copy import deepcopy
from ingredients import render_ingredients
from fluent_compiler.bundle import FluentBundle
import shutil
from decimal import Decimal
import sys
import os

def is_list(value):
    return isinstance(value, list)

def partition_eq(items: [], equal_chunk_size: int):
    if len(items) <= equal_chunk_size:
        return [items]
    else:
        return list(zip(*[iter(items)]*equal_chunk_size))

def partition_2(items: []):
    if len(items) <= 2:
        return [items]
    
    start = 0
    end = len(items)
    mid = int(round(end/2))

    partitions = [
        items[start:mid],
        items[mid:end],
    ]

    return partitions

from jinja2 import pass_context

@pass_context
def msg(context, message_id, args = {}, lang_code = None):
    """
    """
    if lang_code == None:
        lang_code = context["lang_code"]
    
    message = ""

    try:
        message_tmp = data["voc"][message_id]
    except KeyError as e:
        raise KeyError(e)
    
    if message_tmp:
        message = message_tmp
        
        is_function = hasattr(message, "__call__")

        if is_function:
            try:
                message = message(args)
            except KeyError as e:
                raise KeyError(e)
    
    return message

src_dir = os.path.abspath(__file__)
src_dir = os.path.dirname(src_dir)

def template_env(sub_dir: str):
    loader = jinja2.FileSystemLoader(searchpath=src_dir + "/templates/" + sub_dir)
    env = jinja2.Environment(loader=loader)
    env.trim_blocks = True
    env.globals['msg'] = msg
    env.globals['Decimal'] = Decimal

    env.filters.update({
            'is_list': is_list,
            'render_ingredients': render_ingredients,
            'partition_eq': partition_eq,
            'partition_2': partition_2,
    })

    return env

def render_demo(public_dir = "demo"):
    # Shop
    env = template_env("shop")
    template = env.get_template("index.html")
    html = template.render(data)
    html_file = open(public_dir + "/shop/index.html", "w")
    html_file.write(html)

    # Editor
    env = template_env("editor")
    pages = ["index", "products", "bar"]
    for page in pages:
        template = env.get_template(page + ".html")
        html = template.render(data)
        html_file = open(public_dir + "/editor/" + page + ".html", "w")
        html_file.write(html)

def main(argv):
    if len(argv) == 3:
        if argv[1] == "demo":
            render_demo(argv[2])
    else:
        print("Usage: frontend_demo.py demo [PUBLIC_DIR]")

if __name__ == "__main__":
	main(sys.argv)

