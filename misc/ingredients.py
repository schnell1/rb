from copy import deepcopy

def render_ingredients(ingredients: [dict], separation: str = "comma", html_tags=True, input_fields=True):
    """
    separation = "comma" | "list"
    """

    class Deep():
        """
        Wrapper for flattening a dict by key.
        We just store the nesting depth along with the item.
        """
        def __init__(self, item, depth: int = None):
            self.item = item
            self.depth = depth
    
    def to_deep_list(items, depth):
        return [Deep(item, depth) for item in items]

    if len(ingredients) == 0:
        return ""

    items = deepcopy(ingredients)
    key = "ingredients"
    
    left = to_deep_list(items, 0)
    flatten = []

    # Create ordered flattened list `flatten`
    while len(left) >= 1:
        curr = left.pop(0)
        if key in curr.item:
            left = to_deep_list(curr.item[key], curr.depth + 1) + left
        flatten += [curr]
    
    items = flatten
    amount = len(items)
    many = amount >= 2
    parens_open = 0
    
    html = ""
    comma = ", "

    if html_tags:
        match separation:
            case 'comma':
                html += "<p class=\"ingredients-list\">"
            case "list":
                html += "<ul class=\"ingredients-list\">"

    def open_item(item, separation = separation, open_nest = False, prev_same_depth=False, html_tags=html_tags):
        html = ""
        match separation:
            case "comma":
                if prev_same_depth:
                    html += ", "
                if open_nest:
                    html += " ("
                
                if html_tags:
                    html += "<span class=\"ingredient"
                    if "slug" in item:
                        html += " " + item["slug"]
                    html += "\">" + item["name"] + "</span>"
                else:
                    html += item["name"]
            case "list":
                if html_tags:
                    html += "<li class=\"ingredient" + item["name"]
                    if "slug" in item:
                        html += " " + item["slug"]
                    html += "\">" + item["name"] + "</li>"
                else:
                    html += item["name"]
        
        return html

    def close_item(item, last_nest = False, separation = separation, html_tags=html_tags):
        html = ""
        match separation:
            case "comma":
                if html_tags:
                    html += ""
                if last_nest:
                    html += ")"
            case "list":
                if html_tags:
                    html += ""
        return html
    
    for i, curr in enumerate(items):
        is_first = i == 0
        is_last = i == amount - 1
        is_between = not (is_first and is_last)
        
        is_allergenic = False
        if "allergenic" in curr.item:
            if curr.item["allergenic"]:
                is_allergenic = True
        
        allergens = []
        if "allergens" in curr.item:
            if len(curr.item["allergens"]) >= 1:
                allergens = curr.item["allergens"]

        has_claims = False
        if "claims" in curr.item:
            pass
        
        # first item
        if is_first:
            html += open_item(curr.item)
        elif is_last or is_between:
            prev = items[i-1]

            # prev is parent
            if prev.depth < curr.depth:
                parens_open += 1
                html += open_item(curr.item, open_nest=True)
            # else: prev.depth == curr.depth
            else:
                html += open_item(curr.item, prev_same_depth=True)
        
        # close with ")"
        if is_last:
            for i in range(0, parens_open):
                html += close_item(curr.item, last_nest=True)        
        elif is_between:
            head = items[i+1]
            for depth in range(head.depth, curr.depth):
                parens_open -= 1
                html += close_item(curr.item, last_nest=True)
            
    if html_tags:
        match separation:
            case 'comma':
                html += "</p>"
            case "list":
                html += "</ul>"
    return html
