#!/usr/bin/python3
import asyncio
from watchfiles import awatch, Change
from pathlib import Path
import sys
import psycopg2
from urllib.parse import urlparse

async def main():
    media_volume = sys.argv[1]
    database_url = sys.argv[2]

    db = urlparse(database_url)
    db = {
        'dbname': db.path[1:],
        'user': db.username,
        'password': db.password,
        'port': db.port,
        'host': db.hostname
    }

    conn = psycopg2.connect(**db)

    conn.autocommit = True
    cursor = conn.cursor()

    sql_file = open("queries/media.upsert.many.sql")
    sql = sql_file.read()
    sql_file.close()

    async for changes in awatch(media_volume):
        files = []
        for (change, path) in changes:
            match change:
                case Change.added | Change.modified:
                    if Path(path).is_file():
                        base_position = path.rfind(media_volume) + len(media_volume) + 1
                        path = path[base_position:].split('/')
                        paths += [{"path": path}]

        print(paths)
        print(cursor.mogrify(sql, (paths)))
        results = cursor.fetchall()
        print(results)
        conn.commit()
    
    conn.close()

asyncio.run(main())
