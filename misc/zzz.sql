SELECT
    w_products.code,
    products.base_code,
    w_products.path,
    products.ean,
    w_products.unit_code,
    products.base_value,
    w_products.texts,
    w_products.properties,
    products.published,
    products.published_at
FROM
    (
        WITH RECURSIVE
            w_products AS (
                SELECT
                    base_products.code,
                    array[]::varchar[] path,
                    base_products.unit_code
                FROM
                    products base_products
                WHERE
                    base_products.base_code IS NULL

                UNION ALL

                SELECT
                    products.code,
                    path || w_products.code,
                    w_products.unit_code
                FROM
                    products
                JOIN
                    w_products
                ON
                    w_products.code = products.base_code
                WHERE
                    products.base_code IS NOT NULL
            )
        SELECT
            w_products.code,
            w_products.path,
            w_products.unit_code,

            coalesce(
                json_strip_nulls(
                    json_object_agg(
                        translation_code, json_build_object(
                            'name', texts.name,
                            'short_description', texts.short_description,
                            'long_description', texts.long_description,
                            'published', texts.published,
                            'published_at', texts.published_at
                        )
                    ) FILTER (WHERE texts.translation_code IS NOT NULL)
                )
            , '{}'::json) texts,
            coalesce(
                json_strip_nulls(json_agg(json_build_object(
                    'code', products_properties.property_code,
                    'value', products_properties.value
                )) FILTER (WHERE products_properties.property_code IS NOT NULL)),
                '[]'
            ) properties
        FROM
            w_products
        LEFT JOIN
            (
                SELECT
                    *
                FROM
                    products_texts
            ) AS texts
        ON
            texts.product_code = w_products.code
        LEFT JOIN
            products_properties
        ON
            products_properties.product_code = w_products.code
        GROUP BY
            w_products.code,
            w_products.path,
            w_products.unit_code
        ORDER BY
            w_products.path
    ) AS w_products
JOIN
    products
ON
    products.code = w_products.code
;