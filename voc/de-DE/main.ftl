language = Sprache
name = Name
short-description = Kurzbeschreibung
long-description = Langbeschreibung
description = Beschreibung
editor = Editor
data-groups = Datengruppen
products = Produkte
product-set = Produkgruppen
groups = Gruppen
group = Gruppe
amount = Menge
price-gross = Bruttopreis
price-net = Nettopreis
price-sub-total = Zwischensumme
price-net-total = Nettogesamtbetrag
price-min = Mindestpreis
weight-min = Mindestgewicht
cost-type = Kostenart
value = Wert
paramter = Parameter
plus-taxes =
    zzgl. {$count ->
        [one] Steuer
        *[other] Steuern
    }
order = Bestellung
finish-order = Bestellung abschließen
describes-itself-with = beschreibt sich mit
randomize = würfeln
randomize-all = alles würfeln
or-instead = oder stattdessen
with-profile = mit Profil
and-either-with = und entweder mit
box-name = BoxName
box-content = BoxInhalt
bars =
    {$count
        [one] Riegel
        [other]  Riegel
    }
recalc-by-bars-amount = Neu berechnen nach RiegelMengen
share = teilen
box-per-link = Box per Link
or = oder
copy-public-url = Öffentlichen Link teilen
order-now = jetzt bestellen
begin-order = Bestellung beginnen
bar-profile = RiegelProfil
nutritional-values =
    {$count ->
        [one] Nährwert
        [other] Nährwerte
    }
per-value-unit = pro $value $unit
per = pro
ingredients = Zutaten
preview = Vorschau
media = Medien
